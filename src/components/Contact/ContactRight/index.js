const ContactRight = ()=>{
    return(
        <div className="contactForm__right" id="contact">
          <div className="contact__form--tel"><img src="img/contact.png" width="80px" height="80px" alt="contact"/><p>johndoe@gmail.com +32 123 123 123</p></div>
          <img src="img/portret.jpg" width="250px" height="300px" alt="portret"/>
          <p>author:John Doe</p>
          <p>username:@JohnDoe</p>
          <p>university:University Graduate|Software Engineer</p>
          <p>homepage:JohnDoe.github.com</p>
          <p>repository type:Open Source</p>
          <p>url:GitHub.com/JohnDoe</p>
        </div>
    )
}

export default ContactRight;
import {useState} from 'react'

const ContactLeft = ()=>{
let [email,setEmail]=useState('');
let [name,setName]=useState('');
let [msg,setMsg]=useState('');

const handleSubmit=(e)=>{
e.preventDefault();
alert("Thank you for txt to me, I answer soon as possible.");
}

    return(
<div className="contactForm__left">
          <h2>{String.fromCharCode(47)+String.fromCharCode(47)}Contact me</h2>
          <p>
            If you are willing to work with me, contact me. I can join your
            conference to serve you with my engeneering experience.
          </p>
          <form className="contactForm__left-form" onSubmit={(e)=>{handleSubmit(e)}}>
            <input type="email" className="contactForm__left--input" placeholder="Your e-mail" value={email} onChange={(e)=>{setEmail(e.target.value)}}/><br/>
            <input type="text" className="contactForm__left--input" placeholder="Your name" value={name} onChange={(e)=>{setName(e.target.value)}}/><br/>
            <textarea className="contactForm__left--input contactForm__left--textarea" placeholder="How I can help You? Please put here your message/request"
            value={msg} onChange={(e)=>{setMsg(e.target.value)}}></textarea>
            <input type="submit" className="button" value="Send"/>
          </form>
        </div>
    )
}

export default ContactLeft;
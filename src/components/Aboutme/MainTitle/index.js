const MainTitle=()=>{
    return(
<div className="aboutme__main-title">
          <h1>{String.fromCharCode(47)+String.fromCharCode(47)}Hi, my name is John Doe</h1>
       <h3>Software Engineer</h3>
           <p>
             Pasionate techy and tech autor<br />
             with 3 years experience within the field.
           </p>
          <div className="aboutme__main-title-git">
            <p>See my works</p>
            <a href="github.com"><div className="icon github"></div></a>
            <a href="bitucket.com"><div className="icon bitbucket"></div></a>
         </div>
         </div>
    )
}

export default MainTitle;
import Skill from './Skill'

const Skills = ()=>{
    return(
<div className="aboutme__right-skills" id="skills">
            <div className="aboutme__right--title">
              <h2>{String.fromCharCode(47)+String.fromCharCode(47)}Skills</h2>
              <p>
                Laudantium nulla obcaecati doloremque minima fugiat. Dicta
                ratione omnis laboriosam quam aliquam quis.
              </p>
            </div>
            <Skill skill="php" txt="PHP 100%"/>
            <Skill skill="js" txt="JS 90%"/>
            <Skill skill="html" txt="HTML 90%"/>
            <Skill skill="nodejs" txt="NODE JS 60%"/>
            <Skill skill="css" txt="CSS 90%"/>
            <Skill skill="go" txt="GO 60%"/>

          </div>
    )
}

export default Skills;
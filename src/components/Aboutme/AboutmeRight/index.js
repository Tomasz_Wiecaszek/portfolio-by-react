import Freelancer from './Freelancer'
import Skills from './Skills'

const AboutmeRight=()=>{
    return(
    <div className="aboutme__right">
        <Freelancer/>
        <Skills/>
          
        </div>
    
    )
}

export default AboutmeRight;
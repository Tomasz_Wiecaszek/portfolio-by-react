

const AboutmeLeft=()=>{
    return(
    <>
       <div className="aboutme__left">
          <div className="aboutme-circle1"></div>
          <div className="aboutme-photo"></div>
          <div className="aboutme-circle2"></div>
          <div className="aboutme__aboutme">
            <h2>{String.fromCharCode(47)+String.fromCharCode(47)}About me</h2>
            <h3>All about Techy</h3>
            <p>Lorem ipsum dolor sit amet,</p>
            <p>
              consectetur adipisicing elit.consectetur adipisicing elit.
              consectetur adipisicing elit. consectetur adipisicing elit.
              consectetur adipisicing elit. consectetur adipisicing elit.
              consectetur adipisicing elit.consectetur adipisicing elit.
              consectetur adipisicing elit. consectetur adipisicing elit.
              consectetur adipisicing elit. consectetur adipisicing elit.
            </p>
            <p>
              consectetur adipisicing elit.consectetur adipisicing elit.
              consectetur adipisicing elit. consectetur adipisicing elit.
              consectetur adipisicing elit. consectetur adipisicing elit.
            </p>
            <h3>My interest</h3>
            <ul>
              <li>music</li>
              <li>kitesurfing</li>
              <li>cycling</li>
            </ul>
            <div className="aboutme__aboutme-easyCode">
              <p>Ukończyłem kurs Easy-code</p>
              <img src='/easy_code_button.png' className="aboutme__aboutme-photo" alt="EC" />
            </div>
          </div>
        </div>
        
        
          </>
    )

}

export default AboutmeLeft;
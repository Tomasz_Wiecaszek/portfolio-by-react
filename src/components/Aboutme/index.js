import React from 'react'
import MainTitle from './MainTitle'
import AboutmeLeft from './AboutmeLeft'
import AboutmeRight from './AboutmeRight'

const Aboutme =()=>{
    return(
<div className="aboutme" id="aboutme">
        <MainTitle/>
        <AboutmeLeft/>
        <AboutmeRight/>
 </div>
    )
}

export default Aboutme;
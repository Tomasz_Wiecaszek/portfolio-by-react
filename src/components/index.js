
import NavWrapper from './NavWrapper'
import Aboutme from './Aboutme'
import Portfolio from './Portfolio'
import Blog from './Blog'
import Contact from './Contact'
import BackButton from './ButtonBack'
import MobileMenu from './MobileMenu'



const components={
NavWrapper,
Aboutme,
Portfolio,
Blog,
Contact,
BackButton,
 MobileMenu
}

export default components;
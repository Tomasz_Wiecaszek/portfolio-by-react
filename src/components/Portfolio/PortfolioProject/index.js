import {useState} from 'react'

const PortfolioProject =({numb,title,describe,tools })=>{
let [opacity,setOpacity]=useState(0);


    return(
<div className="portfolio__project" onMouseOver={()=>{setOpacity(1)}} onMouseOut={()=>{setOpacity(0)}}>
              <div className={`portfolio__project--article pro${numb}`}>
                <div className="portfolio__project--content" style={{opacity:opacity}}>
                  <h2>{title}</h2>
                  <h3>{describe}</h3>
                  <h3>{tools}</h3>
                </div>
              </div>
              <div className="portfolio__project--bar">
                <a href="https://bitbucket.org/Tomasz_Wiecaszek"
                  ><img className="portfolio__project--ico"
                    src="img/github_icon.png" alt="icon"
                     /></a
                ><a href="https://bitbucket.org/Tomasz_Wiecaszek"
                  ><img className="portfolio__project--ico"
                    src="img/external_link_icon.png" alt="icon"/></a>
              </div>
            </div>
    )
}

export default PortfolioProject;
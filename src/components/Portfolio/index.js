import PortfolioProgram from './PortfolioProgram'
import PortfolioProject from './PortfolioProject'

const Portfolio =()=>{
    return(
        <div className="portfolio" id="portfolio">
        <div className="aboutme-circle1"></div>
        <div className="aboutme-circle2"></div>

        <div className="portfolio__tools">
          <h2>{String.fromCharCode(47)+String.fromCharCode(47)}Tools</h2>
          <h3>My essentials</h3>
        <div className="portfolio__programs">
          <PortfolioProgram tool="react" txt="React" ver="16.6.3"/>
          <PortfolioProgram tool="webpack" txt="Webpack" ver="4.19.1"/>
          <PortfolioProgram tool="js_ico" txt="Express" ver="4.16.3"/>
          <PortfolioProgram tool="sc" txt="Styled Components" ver="4.11"/>
          <PortfolioProgram tool="redux" txt="Redux" ver="4.0.1"/>
          <PortfolioProgram tool="css_ico" txt="Flexbox" ver="4.1.1"/>
          <PortfolioProgram tool="other" txt="Program" ver="1.1"/>
          <PortfolioProgram tool="other" txt="Program" ver="1.1"/>

          
        </div>
        </div>
        <div className="portfolio__portfolio">
          <h2>{String.fromCharCode(47)+String.fromCharCode(47)}My work</h2>
          <h3>Portfolio</h3>
          <br />
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
            Similique,<br />
            quo eaque iure non enim, ullam nostrum neque, tempore odit beatae
            fuga<br />
            voluptates eligendi veritatis. Autem delectus nisi iusto laboriosam
            rerum.
          </p>
          <div className="portfolio__projects">

              <PortfolioProject numb="1" title="Office softwara" describe="Task mangament" tools="Tools: react, redux, scss, js"/>
              <PortfolioProject numb="2" title="Office softwara" describe="Task mangament" tools="Tools: react, redux, scss, js"/>
              <PortfolioProject numb="3" title="Office softwara" describe="Task mangament" tools="Tools: react, redux, scss, js"/>
              <PortfolioProject numb="4" title="Office softwara" describe="Task mangament" tools="Tools: react, redux, scss, js"/>
              <PortfolioProject numb="5" title="Office softwara" describe="Task mangament" tools="Tools: react, redux, scss, js"/>
              <PortfolioProject numb="6" title="Office softwara" describe="Task mangament" tools="Tools: react, redux, scss, js"/>



          </div>
        </div>
      </div>
    )
}

export default Portfolio;
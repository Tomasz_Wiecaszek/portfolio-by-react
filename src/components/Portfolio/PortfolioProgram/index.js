const PortfolioProgram=({tool, txt,ver})=>{
    return(

        <div className="portfolio__programs-ico">
        <div className={`${tool}`}></div>
        <p>{txt}<br />{ver}</p>
      </div>

    )
}

export default PortfolioProgram;
import MobileMenuItem from './MobileMenuItem'
import {React,useState} from 'react'

const MobileMenu=({aboutme, portfolio, blog, contact})=>{

const menuContent=[
    [aboutme,"About Me"],
    [portfolio,"Skills"],
    [portfolio,"Portfolio"],
    [blog,"Blog"],
    [contact,"Contact"]
]

let [opacity,setOpacity]=useState(0);

const showMenu=()=>{
    if(opacity===0){
setOpacity(1)}
else{setOpacity(0)};
}


    return(<>
<div className="mobile-menu__hamburger" onClick={showMenu}>
        <div className="mobile-menu__hamburger--line"></div>
        <div className="mobile-menu__hamburger--line"></div>
        <div className="mobile-menu__hamburger--line"></div>
      </div>

      <div className="mobile-menu__nav" style={{opacity:opacity}}>
         {
             menuContent.map((el)=>{
                return(
                    <MobileMenuItem dest={el[0]} txt={el[1]} key={el[1]} setOpacity={setOpacity}/>
                )
             })
         }

      </div>
      </>
    )
}

export default MobileMenu;
import {Link} from 'react-router-dom'


const MobileMenuItem=({dest,txt,setOpacity})=>{
    const scrollToRef=ref=>{
        window.scrollTo(0,ref.current.offsetTop)
    };


    return(
<p className="mobile-menu__nav--item" onClick={()=>{setOpacity(0)}}><Link to={dest} onClick={()=>{scrollToRef(dest)}}>{txt}</Link></p>
    )
}

export default MobileMenuItem;
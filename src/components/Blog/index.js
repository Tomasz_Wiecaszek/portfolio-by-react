import BlogPost from './BlogPost'

const Blog=()=>{
    return(
        <div className="blogAndContact" id="blog">
        <div className="blogAndContact__blog">
          <h2>{String.fromCharCode(47)+String.fromCharCode(47)}Blog posts</h2>
          <h3>Hits and tips</h3>

<BlogPost numb="1"
 author="author, 01.01.2002 r."
  title1="//Title1" 
  title2="Secondary title" 
  article="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Natus
  placeat blanditiis aliquam quidem iure impedit at, explicabo
  commodi fuga delectus tempore quis, animi quam maxime
  consectetur! Adipisci aut cumque aliquid."/>

<BlogPost numb="2"
 author="author, 01.01.2002 r."
  title1="//Title1" 
  title2="Secondary title" 
  article="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Natus
  placeat blanditiis aliquam quidem iure impedit at, explicabo
  commodi fuga delectus tempore quis, animi quam maxime
  consectetur! Adipisci aut cumque aliquid."/>



        </div>
      </div>
    
    )
}

export default Blog;
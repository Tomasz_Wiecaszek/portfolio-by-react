const BlogPost=({numb, author, title1,title2,article})=>{
    return(
<div className="blogAndContact__blog--post">
            <div className={`blogAndContact__blog--photo bphoto${numb}`}>1</div>
            <div className="blogAndContact__blog--content">
              <div className="blogAndContact__blog--author">
                <i>{author}</i>
              </div>
              <h2>{title1}</h2>
              <h3>{title2}</h3>
              <p>
               {article}
              </p>
              <div className="blogAndContact__blog--readmore">
                <a href="https://bitbucket.org/Tomasz_Wiecaszek"><i>Read more </i></a>
              </div>
            </div>
          </div>
    )
}

export default BlogPost;
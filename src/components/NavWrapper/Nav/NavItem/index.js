import {Link} from 'react-router-dom'

const NavItem=({txt,dest})=>{
    console.log(dest)
const scrollToRef=ref=>{
    window.scrollTo(0,ref.current.offsetTop)
};


    return(
<div className="navigation__menu-link"><Link to ={dest} onClick={()=>{scrollToRef(dest)}}>{txt}</Link></div>
    )
}

export default NavItem;
import NavItem from './NavItem'
import NavIcon from './NavIcon'

const Nav=({aboutme,portfolio,blog,contact})=>{
    return(
<div className="navigation__menu">
          <div className="navigation__menu-links">
            <NavItem txt="About me" dest={aboutme}/>
            <NavItem txt="Skills" dest={portfolio}/>
            <NavItem txt="Portfolio" dest={portfolio}/>
            <NavItem txt="Blog" dest={blog}/>
            <NavItem txt="Contact me" dest={contact}/>
            
          </div>
          <div className="navigation__menu-icons">
          <NavIcon icon="twitter"/>
          <NavIcon icon="facebook"/>
          <NavIcon icon="linkedin"/>
          </div>
        </div>
    )
}

export default Nav;
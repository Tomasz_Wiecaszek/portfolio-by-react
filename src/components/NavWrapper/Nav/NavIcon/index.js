const NavIcon=({icon})=>{
    return(
            <div className="navigation__menu-icon">
              <a href="https://bitbucket.org/Tomasz_Wiecaszek"><i className={`demo-icon icon-${icon}`}></i></a>
            </div>

    )
}

export default NavIcon;
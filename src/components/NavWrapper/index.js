
import Nav from './Nav'

const NavWrapper=( {aboutme,portfolio,blog,contact})=>{
    return(
        <div className="navigation">
             <p className="navigation__logo">J.D.</p>
     <Nav aboutme={aboutme} portfolio={portfolio} blog={blog} contact={contact}/>
        </div>
    )
}

export default NavWrapper;
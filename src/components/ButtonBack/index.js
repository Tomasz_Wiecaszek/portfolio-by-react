import {React,useState} from 'react'
import useBackButton from './useBackButton'

const BackButton =()=>{
    let [showButton,setShowButton]=useState(0);
 const [ShowBackButton,scrollUp]=useBackButton();

 ShowBackButton(setShowButton);


    return(<div className="back-button__button" style={{opacity:showButton}} onClick={scrollUp}>
    <div className="back-button__button--item"></div>
  </div>)
}

export default BackButton;
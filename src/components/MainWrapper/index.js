import {BrowserRouter as Router} from 'react-router-dom'
import {React, useRef} from 'react'
import components from '../'

const MainWrapper =()=>{
const {BackButton,MobileMenu,NavWrapper,Aboutme,Portfolio,Blog,Contact} = components;

const aboutme=useRef(null);
const portfolio=useRef(null);
const blog=useRef(null);
const contact=useRef(null);

    return(
        <Router>
<div className="main_wrapper">
<BackButton/>
<MobileMenu  aboutme={aboutme} portfolio={portfolio} blog={blog} contact={contact}/>
<NavWrapper  aboutme={aboutme} portfolio={portfolio} blog={blog} contact={contact}/>
<div ref={aboutme}><Aboutme /></div>
<div  ref={portfolio}><Portfolio/></div>
<div  ref={blog}><Blog/></div>
<div ref={contact}><Contact /></div>
</div>
</Router>
    )
}

export default MainWrapper;